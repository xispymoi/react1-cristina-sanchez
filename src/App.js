import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import "./App.css";

import Navbar from "./components/Navbar";
import TodoList from "./components/TodoList";
import Card from "./components/Card";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";

let NEXT_ID = 1;

class App extends Component {
  state = {
    filterBy: "",
    todos: [],
  };

  render() {
    return (
      <BrowserRouter>
        <Navbar
          whenSearches={(text) => {
            this.setState((prevState) => ({
              ...prevState,
              filterBy: text,
            }));
          }}
        />

        <Switch>
          <Route
            path="/new-todo"
            render={(props) => {
              return (
                <Card
                  {...props}
                  newTask={(text) => {
                    const newTodo = {
                      id: NEXT_ID,
                      description: text,
                      completed: false,
                    };
                    NEXT_ID += 1;
                    this.setState((prevState) => ({
                      ...prevState,
                      todos: [...prevState.todos, newTodo],
                    }));
                  }}
                />
              );
            }}
          ></Route>
          <Route path="/completed-todos">
            <TodoList
              header="Inbox"
              todos={this.state.todos.filter((todo) => todo.completed === true)}
              filterBy={this.state.filterBy}
              handlerClick={(clickedTodo) => {
                this.setState((prevState) => {
                  const newTodos = prevState.todos.map((todo) => {
                    // busco el todo con el mismo id que el que me está diciendo el componente
                    // <TodoList /> (el clickedTodo que me está dando como argumento)
                    if (todo.id === clickedTodo.id) {
                      // me aseguro que la copia que devuelvo tiene completed: true porque
                      // eso es lo que me está diciendo <TodoList /> al llamar a esta función onComplete
                      return { ...todo, completed: false };
                    }
                    // en cambio devuelvo el todo sin modificar
                    return todo;
                  });

                  return {
                    ...prevState,
                    todos: newTodos,
                  };
                });
              }}
            />
          </Route>
          <Route path="/">
            <TodoList
              header="Inbox"
              todos={this.state.todos.filter(
                (todo) => todo.completed === false
              )}
              filterBy={this.state.filterBy}
              handlerClick={(clickedTodo) => {
                this.setState((prevState) => {
                  const newTodos = prevState.todos.map((todo) => {
                    // busco el todo con el mismo id que el que me está diciendo el componente
                    // <TodoList /> (el clickedTodo que me está dando como argumento)
                    if (todo.id === clickedTodo.id) {
                      // me aseguro que la copia que devuelvo tiene completed: true porque
                      // eso es lo que me está diciendo <TodoList /> al llamar a esta función onComplete
                      return { ...todo, completed: true };
                    }
                    // en cambio devuelvo el todo sin modificar
                    return todo;
                  });

                  return {
                    ...prevState,
                    todos: newTodos,
                  };
                });
              }}
            />
          </Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
