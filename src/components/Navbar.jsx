import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faCamera,
  faPlus,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";

import "../styles/Navbar.scss";

/*
  Comportamiento esperado:

  El componente que utilice a Navbar, tiene que pasarle un atributo (prop)
  llamado "whenSearches" cuyo valor tiene que ser una función, esta función
  se va ejecutar cada vez que el usuario escriba en el campo de búsquedas,
  y como primer argumento recibirá el string del texto de búsqueda que ha
  escrito el usuario.
*/
export default class Navbar extends Component {
  render() {
    return (
      <div className="Navbar">
        <div className="Navbar__left">
          <span className="Navbar__svg">
            <Link to="/">
              <FontAwesomeIcon className="Navbar__icon" icon={faHome} />
            </Link>
          </span>
          <div className="Navbar__search">
            <input
              type="text"
              placeholder="Search"
              name="filter"
              className="Navbar__input"
              onChange={(event) => {
                this.props.whenSearches(event.target.value);
              }}
            />
            <button type="submit" className="Navbar__searchButton">
              <i>
                <FontAwesomeIcon className="Navbar__icon" icon={faSearch} />
              </i>
            </button>
          </div>
        </div>
        <div className="Navbar__right">
          <span className="Navbar__svg">
            <Link to="/new-todo">
              <FontAwesomeIcon className="Navbar__icon" icon={faPlus} />
            </Link>
          </span>
          <span>
            <Link to="/completed-todos">Done!</Link>
          </span>
        </div>
      </div>
    );
  }
}
