import React, { Component } from "react";
import "../styles/Card.scss";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class Card extends Component {
  state = {
    newTaskDescription: "",
  };

  render() {
    return (
      <div className="Card">
        <div className="Card__title">
          <h3> Add your new task </h3>
        </div>
        <div className="Card__input">
          <input
            type="text"
            placeholder="Write your new task"
            value={this.state.newTaskDescription}
            onKeyPress={(event) => {
              if (event.charCode === 13) {
                this.props.newTask(this.state.newTaskDescription);
                this.setState({ newTaskDescription: "" });
                this.props.history.push("/");
              }
            }}
            onChange={(event) => {
              this.setState({
                newTaskDescription: event.target.value,
              });
            }}
          ></input>
          <button
            onClick={() => {
              this.props.newTask(this.state.newTaskDescription);
              this.setState({ newTaskDescription: "" });
              this.props.history.push("/");
            }}
          >
            Add task
          </button>
          <button onClick={() => this.setState({ newTaskDescription: "" })}>
            Cancel
          </button>
        </div>
        <div className="Card__img">
          <img
            src="https://d12y7sg0iam4lc.cloudfront.net/s/img/marketing/top-todo-app/to-do-list.png"
            alt="imagen to do"
          ></img>
        </div>
      </div>
    );
  }
}
