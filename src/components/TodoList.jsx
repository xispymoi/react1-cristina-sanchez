import React, { Component } from "react";
import "../styles/TodoList.scss";
import Card from "../components/Card";
import { Link } from "react-router-dom";

export default class TodoList extends Component {
  render() {
    const todos = this.props.todos.filter(
      (todo) =>
        todo.description
          .toUpperCase()
          .trim()
          .includes(this.props.filterBy.toUpperCase().trim())
      //Para que en la búsqueda no influya si es mayúscula, minúscula o hay espacios//
    );

    const todosList =
      todos.length === 0 ? (
        <div className="TodoList__done">
          <h3>Free up your mental space.</h3>
          <span className="TodoList__Start">
            <Link className="TodoList__button" role="button" to="/new-todo">
              Start!
            </Link>
          </span>
        </div>
      ) : (
        <ul className="TodoList__ul">
          {todos.map((todo) => (
            <li key={todo.id}>
              <input
                className="TodoList__input"
                type="checkbox"
                onChange={() => this.props.handlerClick(todo)}
              />{" "}
              {todo.description}
            </li>
          ))}
        </ul>
      );

    return (
      <div className="TodoList">
        <img
          className="TodoList__img"
          src="https://kit8.net/images/thumbnails/580/386/detailed/3/To_do_list@2x.png"
          alt="Imagen To Do list"
        ></img>
        <h3>{this.props.header}</h3>
        {todosList}
      </div>
    );
  }
}
